#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import traceback   # for debug

from enum import Enum
 
class CsvParsingState(Enum):
    Start = 0
    NormalCell = 1
    QuotedCell = 2
    QuotedCellEncounterQuote = 3

class Csv:
    rows = [] 

    def dump(self, splitter = "|"):
        for one_row in self.rows:
            first_cell = 1
            for one_cell in one_row:
                if (first_cell):
                    print("%s" % one_cell, end = "")
                    first_cell = 0
                else:
                    print("%s%s" % (splitter, one_cell), end = "")
            print("");


    def parse(self, raw):
        #print("%s" % raw)

        rows = [];
        current_row = []
        current_cell= ""

        state = CsvParsingState.Start

        for ch in raw:
            if (state == CsvParsingState.Start):
                # , \n " are special
                if (',' == ch):
                    current_row.append(current_cell)
                elif ('\n' == ch):
                    current_row.append(current_cell)
                    rows.append( current_row) 
                elif ('"' == ch):
                    state = CsvParsingState.QuotedCell 
                else:
                    state = CsvParsingState.NormalCell
                    current_cell += ch
            elif (state == CsvParsingState.NormalCell): 
                # , \n are special
                if (',' == ch):
                    current_row.append(current_cell)
                    current_cell = ""
                    state = CsvParsingState.Start

                elif ('\n' == ch):
                    current_row.append(current_cell)
                    rows.append( current_row) 
                    current_cell = ""
                    current_row = []
                    state = CsvParsingState.Start  
                else:
                    current_cell += ch
            elif (state == CsvParsingState.QuotedCell): 
                # only " is special 
                if ('"' == ch):
                    state = CsvParsingState.QuotedCellEncounterQuote
                else:
                    current_cell += ch
            elif (state == CsvParsingState.QuotedCellEncounterQuote): 
                # only , \n " are valid 
                if (',' == ch):
                    current_row.append(current_cell)
                    state = CsvParsingState.Start
                    current_cell = ""
                elif ('\n' == ch):
                    current_row.append(current_cell)
                    rows.append( current_row) 
                    state = CsvParsingState.Start
                    current_cell = ""
                    current_row = []
                elif ('"' == ch):
                    current_cell += ch
                    state = CsvParsingState.QuotedCell 
                else:
                    r = len(rows) + 1
                    c = len(current_row) + 1 
                    raise Exception ("syntax error on row: %d, cell: %d" % (r,c ))

        if (len(current_cell)):
            current_row.append( current_cell)

        if (len(current_row)):
            rows.append(current_row)
        
        self.rows = rows

def main():
    try: 
        raw = sys.stdin.read()

        csv = Csv()
        csv.parse(raw)
        csv.dump()


    except  Exception as e:
        print(e)
        (t, v, bt) = sys.exc_info()
        traceback.print_exception(t, v, bt)
    
        print("\n%s\n" % raw)
        return 2

    print("")

    return 0

if __name__ == "__main__":
    r = main()
    sys.exit(r)


